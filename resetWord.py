#   29.10.2014		@ Jan S. Appelt
#
#		LaTeX-Tools v0.1
#			resetWord.py
#
#		Woerterbuecher:
#				- vUnitDic 					Woerterbuch der physikalischen Einheiten.
#				- vMeesDic 					Woerterbuch der Groessenordnungen.
#				- vOtherDic					Woerterbuch fuer manche andere Zeichen, die schlicht ersetzt werden muessen.
#				- vGreeDic					Woerterbuch des griechischen Alphabets

#------------------------------------------------Dics

vUnitDic = {'m': '\\meter ', 's': '\\second ', 'g': '\\gram ', 'A': '\\ampere ', 'V': '\\volt ', 'Ω': '\\Omega ', 'ohm': '\\Omega ', 'K': '\\kelvin', 'mol': '\\mol', 'cd': '\\candela', 'J': '\\joule', 'Pa': '\\pascal', 'bar': '\\bar', 'Hz': '\\Hertz', 'W': '\\watt', 'F': '\\farad', 'C': '\\coulomb', 'H': '\\henry', 'T': '\\tesla', 'Wb': '\\Weba'}
vMessDic = {'m': '\\milli ', 'k': '\\kilo ', 'c': '\\centi ', 'M': '\\mega ', 'E': '\\exa', 'P': '\\peta', 'T': '\\tera', 'G': '\\giga', 'h': '\\hekto', 'd': '\\dezi', 'µ': '\\mikro', 'n': '\\nano', 'p': '\\pico', 'f': '\\femto', 'a': '\\atto'}
vOtherDic = {'Δ': '\\increment ', '∂': '\\partial', 'ð': '\\eth', '∇': '\\nabla', '∞': '\\infty', '⌀': '\\diameter', '--': ''}
vGreeDic = {'alpha': '\\alpha', 'beta': '\\beta', 'gamma': '\\gamma', 'delta': '\\delta', 'epsilon': '\\epsilon', 'zeta': '\\zeta', 'eta': '\\eta', 'theta': '\\theta', 'jota': '\\iota', 'kappa': '\\kappa', 'lambda': '\\lambda', 'my': '\\mu', 'ny': '\\nu', 'xi': '\\xi', 'omigron': '\\o', 'pi': '\\pi', 'rho': '\\rho', 'sigma': '\\sigma', 'tau': '\\tau', 'ypsilon': '\\upsilon', 'phi': '\\phi', 'chi': '\\chi', 'psi': '\\psi', 'omega': '\\omega', 'Alpha': 'A', 'Beta': 'B', 'Gamma': '\\Gamma', 'Delta': '\\Delta', 'Epsilon': 'E', 'Zeta': 'Z', 'Eta': 'H', 'Theta': '\\Theta', 'Jota': 'I', 'Kappa': 'K', 'Lambda': '\\Lambda', 'My': '\M', 'Ny': 'N', 'Xi': '\\Xi', 'Omigron': 'O', 'Pi': '\\Pi', 'Rho': 'P', 'Sigma': '\\Sigma', 'Tau': 'T', 'Ypsilon': '\\Upsilon', 'Phi': '\\Phi', 'Chi': 'X', 'Psi': '\\Psi', 'Omega': '\\Omega', 'α': '\\alpha', 'β': '\\beta', 'γ': '\\gamma', 'δ': '\\delta', 'ε': '\\epsilon', 'ζ': '\\zeta', 'η': '\\eta', 'θ': '\\theta', 'ι': '\\iota', 'κ': '\\kappa', 'λ': '\\lambda', 'μ': '\\mu', 'ν': '\\nu', 'ξ': '\\xi', 'ο': '\\o', 'π': '\\pi', 'ρ': '\\rho', 'σ': '\\sigma', 'τ': '\\tau', 'υ': '\\upsilon', 'φ': '\\phi', 'χ': '\\chi', 'ψ': '\\psi', 'ω': '\\omega', 'Γ': '\\Gamma', 'Δ': '\\Delta', 'Θ': '\\Theta', 'Λ': '\\Lambda', 'Ξ': '\\Xi', 'Π': '\\Pi', 'Σ': '\\Sigma', 'Υ': '\\Upsilon', 'Φ': '\\Phi', 'Ψ': '\\Psi', 'Ω': '\\Omega'}

#---------------------------- --------------------Funktionen

def isUnit(pWord):								# Prueft ob die Sequenz ein Einheitssmbol ist.
	vRet = ''

	if pWord in vUnitDic:
		vRet = vUnitDic[pWord]

	return vRet
def isMess(pWord):								# Prueft ob die Sequenz eine Masszahl ist, wie 'centi' oder 'milli'.
	vRet = ''
	
	if pWord in vMessDic:
		vRet = vMessDic[pWord]
	
	return vRet
def isExpo(pWord):								# Prueft ob die Sequenz ein Exponent ist.
	vRet = ''	

	if len(pWord) > 1 and pWord[0] == '^':
		temp = ''
		for i in range(1, len(pWord)):
			temp += pWord[i]
		if temp.isnumeric():
			vRet = '^{' + temp + '}'
	elif pWord == '²' or pWord == '³':
		vRet = pWord
	
	return vRet
def isOther(pWord):								# Prueft ob die Sequenz ein anderes LaTeX-Symbol ist
	vRet = ''

	if pWord in vOtherDic:
		vRet = vOtherDic[pWord]
	elif pWord in vGreeDic:
		vRet = vGreeDic[pWord]

	return vRet

#------------------------ Einzelne Checks

def checkIfUnit(pWord):
	vRet = ''
	i = len(pWord)
	ii = 0	 

	while i >= 0 and vRet == '':						# 1. Schleife,check Expo, von hinten nach vorn
		temp = ''
		for u in range(i, len(pWord)):				# Fasst Wort zusammen
			temp += pWord[u]
		temp = isExpo(temp)
		
		if (temp != '' and i != 0) or (temp == '' and i == 0):		# Prueft ob einzelnes Expo oder garkeines
			if i == 0:
				length = len(pWord)
			else:
				length = i
			while ii < length and vRet == '':				# 2. Schleife, check Unit, von vorn nach hinten
				temp2 = ''								
				for u in range(ii, length):						# Fasst Wort zusammen
					temp2 += pWord[u]
				temp2 = isUnit(temp2)
				
				if temp2 != '' and ii != 0:				# Prueft ob Einheit  und einziges
					temp3 = ''
					for u in range(0, ii):					# Fasst Wort zusammen
						temp3 += pWord[u]
					temp3 = isMess(temp3)

					if temp3 != '':									# Prueft ob Mass
						vRet = temp3 + temp2 + temp
				elif temp2 != '':									# Prueft ob einziges
					vRet = temp2 + temp
				ii += 1
		i -= 1

	return vRet
def checkIfOther(pWord):
	vRet = ''
	i = 0
	vChange = False
	
	while i < len(pWord):										# 1. Schleife von forn nach hinten
		length = len(pWord)
		while length > i:											# 2. Schleife von hinten nach i
			temp = ''
			for u in range(i,length):						# Fasst Wort zusammen
				temp += pWord[u]
			temp2 = isOther(temp)
			
			if temp2 != '':											# Prueft ob Symbol vorhanden
				vRet += temp2														# Fuegt Symbol hinzu
				pWord = pWord.replace(temp, '', 1)			# Ersetzt das Symbol aus dem zu ueberpruefenden Param.
				length = len(pWord) + 1									# Setzt length auf neue laenge
				temp2 = ''
				vChange = True													# Setzt Aenderungsvar.
			elif temp == '^-':												# Prueft ob Ueberstrich, und aendert
				vRet = '\\bar{'+ vRet +'}'
				pWord = pWord.replace(temp, '', 1)
				length = len(pWord) + 1
				vChange = True
			elif temp == '^..':												# Prueft ob Doppelueberpunkt, und aendert
				vRet = '\\ddot{'+ vRet +'}'
				pWord = pWord.replace(temp, '', 1)
				length = len(pWord) + 1
				vChange = True
			elif temp == '^.':												# Preuft ob Ueberpunkt, und aendert
				vRet = '\\dot{'+ vRet +'}'
				pWord = pWord.replace(temp, '', 1)
				length = len(pWord) + 1
				vChange = True

			length -= 1
		if i < len(pWord):										# Prueft obnicht erkanntes passiertes zeichen hinzugefuegt weden muß
			vRet += pWord[i]
		i+= 1
	
	if vChange == False:										# Setzt, falls kein Sybmol gefunden wurde den Return auf null
		vRet = ''

	return vRet

#--------------Ersetzt evt. das Wort fuer LaTeX

def checkWord(pWord):
	vRet = ''	
	
	vRet = checkIfUnit(pWord)
	if vRet != '':															# Nacharbeit, falls Einheit gefunden
		vRet = ''+ vRet +''
	else:																				# Prueft ob anderes Symbol, welches in LaTex anders Dargestellt werden soll.
		vRet = checkIfOther(pWord)
																	
	if vRet == '' and len(pWord) == 1:					# Fuegt entsprechendes LaTeX-Kommando fuer Wort an
		if pWord == '/':
			vRet = ' \\per '
		else:
			vRet = pWord
	elif vRet == '':
		if pWord == '--':													# Fuegt LeereSpalte ein
			vRet = ''
		elif pWord == '&&':												# Fuegt Multicolumn ein
			vRet = pWord
		elif pWord == '+&':												# Fuegt FehlerMulticolumn ein
			vRet = pWord
		else:
			vRet = '\\text{' + pWord + '}'

	return vRet
