#		29.10.2014			@ Jan S. Appelt
#
#		LaTeX-Tools v0.1
#			Tools.py

import resetWord as rW
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import numpy as np

#      private Funktionen
#---------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------

def calculateArithmetic(pValues):		# Berechnet das Arithmetische Mittel der Werte Liste
	vRet = 0
	
	if len(pValues) >= 1:
		for value in pValues:
			vRet += value

		vRet = vRet / len(pValues)
	
	return vRet
def calculateStD(pValues, pArithmetic):			# Errechnet die Standart ABweichung der Werte Liste
	vRet = 0

	if len(pValues) >= 2:
		for value in pValues:
			vRet += (value - pArithmetic)**2
		vRet = (vRet/(len(pValues)-1))**(1/2)

	return vRet
def calculateStDNoArithmetic(pValues):			# # Errechnet die Standart ABweichung der Werte Liste, 
	return calculateStD(pValues, calculateArithmetic(pValues))

def getMinNMax(pList):	
	vMin = 0
	vMax = 0

	if len(pList) >= 1:
		vMin = pList[0]
		vMax = pList[0]
		for v in pList:
			if v > vMax:
				vMax = v
			elif v < vMin:
				vMin = v

	return vMin, vMax
def getHeadLines(pSourthPath):
	vCount = 0								# Zaehlt die relevanten Zeilen
	vRet = []

	with open(pSourthPath, 'r') as File:		# Oeffnet die Ziel Datei
		Lines = File.readlines()							# Liest die zeilen der Datei aus.
	File.close()

	for vCurLi in Lines:										# Durchläuft einmal alle Zeilen
		if len(vCurLi) >= 2 and vCurLi[0] == '#' and vCurLi[1] == '*':
			vSpaceCount = 0
			temp = ''
			vFirstSpaceSeq = True
			vSpaceMod = True
			
			vCurLi = vCurLi.replace('#*', '', 1)
			vRet += [[]]

			for char in vCurLi:
				if char == ' ' or char == '\t':			# Prueft ob Space
					vSpaceMod = True							
					if char == '\t' and vSpaceCount == 0:
						vSpaceCount += 2
					else:
						vSpaceCount += 1
				else:
					if char != '\n':
												# Prueft darauf,ob eine neue Spalte vorliegt.
						if vSpaceMod == True and vSpaceCount >= 2 and vFirstSpaceSeq != True:
							if len(temp) >= 2 and temp[0] == '&' and temp[1] == '&':			# Prueft auf Doppelzeile
								temp = temp.replace('&&', '', 1)													# Ersetzt entsprechende Syntax
								vRet[vCount] += [temp]																			# Speichert schon fuer die erstere Spalte
							
							vRet[vCount] += [temp]
							temp = ''
						
						temp += char		# Fuegt neues Zeichen an
								
						vSpaceCount = 0					# Setzt Settings
						vSpaceMod = False
						vFirstSpaceSeq = False
					else:
						if len(temp) >= 2 and temp[0] == '&' and temp[1] == '&':
							temp = temp.replace('&&', '', 1)
							vRet[vCount] += [temp]
						if temp != '':
							vRet[vCount] += [temp]

			vCount += 1

	return vRet
def transToLaTeX(pTitleMat):
	vRet = []
		
	for vCurLineInd in range(0, len(pTitleMat)):		# Zeilen schleife
		vRet += [[]]
		for vCurCol in pTitleMat[vCurLineInd]:				# Spalten schleife
			temp1 = ''
			temp2 = ''
			for char in vCurCol:												# Zeichen schleifen
				if char != ' ':															# Haengt CurChar an
					temp1 += char
				elif char == ' ':
					temp2 += rW.checkWord(temp1)							# Wertet bis jetzt Zeichen aus
					temp1 = ''
			
			temp2 += rW.checkWord(temp1)									# Wertet zeichen aus
			
			if '\\bar' in temp2:												# Prueft fuer Umklammerung
				temp2 = '$' + temp2 + '$'
			else:
				temp2 = '$\\si{' + temp2 + '}$'

			vRet[vCurLineInd] += [temp2]

	return vRet 
			

#      Funktionale Funktionen
#---------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------

def teXDataTable(pSourcePath, pTargetPath):
	with open(pSourcePath,'r') as File:
		FileText = File.readlines()
	
		MidString = '\\midrule\n'			# String fuer Mittelteil der Tabelle
		TopString = '\\toprule\n'			# String fuer Oberteil der Tabelle
		vRowCount = 0				# Zaehlt die Anzahl der Spaltn
	
		lPreDot = []
		lPostDot = []
		vFai = []
							# Schleife um Zeilen im eingelesenen Text durch zu gehen.
		for line in FileText:	
			temp = ''							# temporaerer String
			vSpaceCount = 0				# Zaehlt die Space's zwischen den zeichen(Auch Tabbs)
			vTempRowCount = 1			# Zaehlt die Spaltenzahl in der aktuellen Zeile
			vFirstSpaceSeq = True	# Gibt an, ob es der ersten evt. SpaceSequenze ist(wird ignoriert)
			vSpaceMode = True			# Gibt an, ob er gerade in einer SpaceSequenz ist(war)
			
			if len(line) >= 1 and line[0] == '#':				#Seperariert die Daten heraus.
				if len(line) >= 2 and line[1] == '*':
					ColTemp = ''#'$\\si{'
					RowTemp = ''
					line = line.replace('#*', '', 1)
					
					for char in line:												# Schleifewelche die Zeichen der Zeile durch geht 
						if char == ' ' or char == '\t':			# Prueft ob Space
							vSpaceMod = True							
							if char == '\t' and vSpaceCount == 0:
								vSpaceCount += 2
							else:
								vSpaceCount += 1
						else:
							if char != '\n':
														# Prueft darauf,ob eine neue Spalte vorliegt.
								if vSpaceMod == True and vSpaceCount >= 2 and vFirstSpaceSeq != True:
									if temp != '':
										RowTemp += rW.checkWord(temp)
										temp = ''
										
										if len(RowTemp) >= 2 and (RowTemp[0] == '&' or RowTemp[0] == '+') and RowTemp[1] == '&':
											ColTemp += ' \\multicolumn{2}{c}{'
											for i in range(2, len(RowTemp)):
												ColTemp += RowTemp[i]
											ColTemp += '} &'
											if RowTemp[0] == '+':
												vFai += [vTempRowCount]
					
											vTempRowCount += 2
										#elif len(RowTemp) >= 2 and RowTemp[0] == '+' and RowTemp[1] == '-':
										else:
											if RowTemp != '':
												if '\\bar{' in RowTemp:
													ColTemp += ' $' + RowTemp + '$ & '
												else:
													ColTemp += ' $\\si{' + RowTemp + '}$ & '
											else:
												ColTemp += ' & ' 
											vTempRowCount += 1

									RowTemp = ''
															# Prueft darauf, ob ein neues Word anfaengt
								if vSpaceCount == 1 and vFirstSpaceSeq != True:
									RowTemp += rW.checkWord(temp)
									temp = ''
									
								if char == '_':
									RowTemp += rW.checkWord(temp) + '_'
									temp = ''
								else:
									temp += char
									
								vSpaceCount = 0
								vSpaceMod = False
								vFirstSpaceSeq = False
							else:
								if temp != '':
									RowTemp += rW.checkWord(temp)
									temp = ''
								
								if len(RowTemp) >= 2 and (RowTemp[0] == '&' or RowTemp[0] == '+') and RowTemp[1] == '&':
									ColTemp += ' \\multicolumn{2}{c}{'
									for i in range(2, len(RowTemp)):
										ColTemp += RowTemp[i]
									ColTemp += '}'
									if RowTemp[0] == '+':
												vFai += [vTempRowCount]

									vTempRowCount += 1
								elif RowTemp != '':
									ColTemp += ' \\si{' + RowTemp + '}'

								ColTemp += ' \\\\\n'
								TopString += ColTemp
																				# Prueft ob aktuelle Spaltenzahl = bis jetzt gezaehlte Spaltenzahl
								if vRowCount == 0:
									vRowCount = vTempRowCount
									for i in range(0, vRowCount):		# initialisiert FormatCounter(zahehlen max. ziffern)
										lPreDot += [0]
										lPostDot += [0]
								elif vRowCount != vTempRowCount:
									print('In den Zeilen ist nicht immer die gleiche Anzahl an Spalten vorhanden.')
									print('In the Coloums arn\'t the same Number of Rows.')
	#----------------------------Mid-Table-Abschnitt
			else:
				vTempPreDot = 0;
				vTempPostDot = 0;
				vDotState = 0
	
				for char in line:					# Schleife in die die einzelnen Zeichen einer Zeile durch gegangen werden
					if char == ' ' or char == '\t':									# Prueft ob Leerzeichen
						vSpaceMod = True					
						
						if char == '\t' and vSpaceCount == 0:					
							vSpaceCount += 2
						else:
							vSpaceCount += 1
					else:
						if char != '\n':									# Prueft ob letztes Zeichen der Zeile erreicht
																							# Prueft darauf,ob eine neue Spalte vorliegt.
							if vSpaceMod == True and vSpaceCount >= 2 and vFirstSpaceSeq != True:
								if vDotState == 0:						# Pruft PreDot Count
									if vTempPreDot > lPreDot[vTempRowCount-1]:
										lPreDot[vTempRowCount-1] = vTempPreDot
									vTempPreDot = 0
								elif vDotState == 1:					# Pruft PostDot Count
									if vTempPostDot > lPostDot[vTempRowCount-1]:
										lPostDot[vTempRowCount-1] = vTempPostDot
									vTempPostDot = 0
								
								temp += ' & ' 
								vDotState = 0
								vTempRowCount += 1
																							# Prueft ob Zahl mit Space	versehen					
							if vSpaceMod == True and vSpaceCount == 1 and vFirstSpaceSeq != True:
								if vDotState == 0:						# Pruft PreDot Count
									if vTempPreDot > lPreDot[vTempRowCount-1]:
										lPreDot[vTempRowCount-1] = vTempPreDot
									vTempPreDot = 0
								elif vDotState == 1:					# Pruft PostDot Count
									if vTempPostDot > lPostDot[vTempRowCount-1]:
										lPostDot[vTempRowCount-1] = vTempPostDot
									vTempPostDot = 0
								vDotState = 2
	
							if vDotState != 2:
								if char == '.' or char == ',':
									if vTempPreDot > lPreDot[vTempRowCount-1]:	# Pruft PreDot Count 
										lPreDot[vTempRowCount-1] = vTempPreDot
									vTempPreDot = 0
									vDotState = 1
								else:
									if vDotState == 0:		
										vTempPreDot += 1
									elif vDotState == 1:
										vTempPostDot += 1
							
							vSpaceCount = 0	
							vSpaceMod = False
							vFirstSpaceSeq = False
							temp += char
						else:
							if vDotState == 0 and vTempPreDot > lPreDot[vTempRowCount-1]:
								lPreDot[vTempRowCount-1] = vTempPreDot
							elif vDotState == 1 and vTempPostDot > lPostDot[vTempRowCount-1]:
								lPostDot[vTempRowCount-1] = vTempPostDot
							
							temp += ' \\\\\n'
							MidString += temp
																				# Prueft ob aktuelle Spaltenzahl = bis jetzt gezaehlte Spaltenzahl
							if vRowCount == 0:
								vRowCount = vTempRowCount
							elif vRowCount != vTempRowCount:
								print('In den Zeilen ist nicht immer die gleiche Anzahl an Spalten vorhanden.')
								print('In the Coloums arn\'t the same Number of Rows.')
							
	
	File.close()
	
	#-------------------------------------------Erstellt neue Datei
	
	tempS = ''
	for i in range(0, vRowCount):
		tempS += ' S[table-format={}.{}]'.format(lPreDot[i], lPostDot[i])
	
	with open(pTargetPath, 'w') as SecFile:
		SecFile.write('\\begin{tabular}{'+ tempS +'}\n'+ TopString + MidString +'\\bottomrule\n\\end{tabular}')
	SecFile.close()

#----------------------------------------------------------------------

def dataEvaluation(pSourcePath, pTargetPath, pSettingsNumber, pMeasurNumber):
	vTempSetVal = []
	vCurSetVal = []
	vTempMeaVal = []
	vCount = 0
	vOutput = ''

	vDatas = np.genfromtxt(pSourcePath, unpack=True)
	
	for i in range(0, pSettingsNumber):							# initialisiert Temp und Cur Set Val
		vTempSetVal += [vDatas[i][0]]
		vCurSetVal += [0]
	for i in range(0, pMeasurNumber):								# initialisiert Temp MeasurVal's
		vTempMeaVal += [[]]

	for i in range(0, len(vDatas[0])):							# Geht einmal die Zeilen der Datansätze durch
		vEqual = True		
		for u in range(0, pSettingsNumber):						# Aktualisiert die CurSetVar's und prueft ob unterschied zu den TempSetVar's
			vCurSetVal[u] = vDatas[u][i]
			if vCurSetVal[u] != vTempSetVal[u]:
				vEqual = False

		if vEqual == False:														# Beginnt Fall, falls Unterschied zwischen Cur und Temp SetVar's
			for u in range(0, pSettingsNumber):						# Setzt die Temp auf die CurSetVar's
				vOutput += '{}\t'.format(vTempSetVal[u])		# Speichert in Output
				vTempSetVal[u] = vCurSetVal[u]

			for u in range(0, pMeasurNumber):						# Speichert Arth und StD
				vOutput += '{}\t'.format(calculateArithmetic(vTempMeaVal[u])) 
				vOutput += '{}\t'.format(calculateStDNoArithmetic(vTempMeaVal[u]))
				vTempMeaVal[u] = [vDatas[u+pSettingsNumber][i]]
			vOutput += '\n'
			
		else:
			for u in range(0, pMeasurNumber):					# Speichert den Wert des Abschnitts
				vTempMeaVal[u] += [vDatas[u+pSettingsNumber][i]]

	for u in range(0, pSettingsNumber):						# Setzt die Temp auf die CurSetVar's
		vOutput += '{}\t'.format(vTempSetVal[u])
		vTempSetVal[u] = vCurSetVal[u]
	
	for u in range(0, pMeasurNumber):
		vOutput += '{}\t'.format(calculateArithmetic(vTempMeaVal[u])) 
		vOutput += '{}\t'.format(calculateStDNoArithmetic(vTempMeaVal[u]))
	
	vTitleMat = getHeadLines(pSourcePath)					# Titel bearbeitung, hier werden eingelesen
	vHead = ''
	for vCurLine in vTitleMat:										# Werden richtig zum String zusammengefuehrt
		vHead += '#*' 
		for i in range(0, len(vCurLine)):
			vCurCol = vCurLine[i]
			if i < (pSettingsNumber + pMeasurNumber):
				if i >= pSettingsNumber:
					vHead += '&& '
				
				vHead += vCurCol 
				vHead += '		'
		vHead += '\n'

	with open(pTargetPath, 'w') as SecFile:
		SecFile.write(vHead + '#\n' +  vOutput)
	SecFile.close()

#---------------------------------------------------------------------------------------------------------------

def plotData(pSourcePath, pTargetName, pSettingsNumber, pMeasurNumber, pMatrix, pError):
	vCount = 0	
	vDatas = np.genfromtxt(pSourcePath, unpack=True)
	vLables = []
	
	vTemp = transToLaTeX(getHeadLines(pSourcePath))
	if len(vTemp) > 1:
		for i in range(0, len(vTemp[len(vTemp)-2])):
			vLables += [vTemp[len(vTemp)-2][i] + ' (in ' + vTemp[len(vTemp)-1][i] + ')']
	else:
		for i in range(0, len(vTemp[0])):
			vLables += [vTemp[0][i]]
	
	for i in range(0, len(pMatrix)):
		for u in range(0, len(pMatrix[i])):
			var = pMatrix[i][u]							# var gibt die Art des Graphen an
			if var != 0:
				if i < pSettingsNumber or pError == False:
					vXValues = vDatas[i]
				else:
					vXValues = vDatas[(i - pSettingsNumber) * 2]
				if u < pSettingsNumber or pError == False:
					vYValues = vDatas[u]
				else:
					vXValues = vDatas[(u - pSettingsNumber) * 2]

				if var >= 1 and var < 2:									# Macht entsprechnden Plot
					plt.plot(vXValues, vYValues, 'k.', label='Messwerte')
					var = var - 1	
				elif var >= 2 and var < 3:
					plt.polar(vXValues, vYValues, 'k.', label='Messwerte')
					var = var - 2
				elif var >= 3 and var < 4:
					plt.hist(vXValues, vYValues, label='Messwerte')
					var = var - 3

				if var == 0.1:														# Scaliert den Plot entsprechend
					plt.xscale('log')
					plt.yscale('log')
				elif var == 0.11:
					plt.xscale('log')
				elif var == 0.12:
					plt.yscale('log')
																									# Lable der Achsen
				plt.xlabel(vLables[i])
				plt.ylabel(vLables[u])

				vMin, vMax = getMinNMax(vXValues)					# Anpassungs des Ausschnittes
				plt.xlim(vMin-((vMax-vMin)/100), vMax+((vMax-vMin)/100))
				vMin, vMax = getMinNMax(vYValues)
				plt.ylim(vMin-((vMax-vMin)/100), vMax+((vMax-vMin)/100))

				plt.legend()
				plt.tight_layout()
				plt.savefig(pTargetName + '{}.pdf'.format(vCount))

				vCount += 1

#									Tests Einstellungen
#---------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------

#vHead = '#* Δt   && R_Probe 	&& R_Geh	 && I     && U'
#vMatrix = [[0, 1.11, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
#plotData('data.txt', 'Plot', 1, 4, vMatrix, False)
#dataEvaluation('data.txt', 'tar.txt', 1, 8, vHead)
teXDataTable('data.txt', 'tar2.txt')
