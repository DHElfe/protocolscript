 Dokumentation
|-------------|

Mit diesem Skript kann der Code fuer eine LaTeX-Tabelle generiert werden. Der entsprechende Code wird in einer extra Datei gespeichert und kann direkt in eine LaTeX-Datei importiert werden.
Die generierte Datei enthält dabei die 'tabular'-Section, es muß also noch die übergeordnete 'table'-Section implementiert werden.

Die eingelesene Datei wird Zeilenweise ausgelesen, und entspricht einer Zeile der Tabelle.
Spalten werden durch mindestens zwei Leerzeichen oder einem Tabulator unterschieden. Zu beachten sei, dass in jeder Zeile die gleiche Anzahl an Spalten zu stehen hat. Ist dem nicht so, so gibt das Programm eine entsprechende Ausgabe, und das Korrekt sein des generierten LaTeX.Codes ist nicht gewährleistet.
Um eine Leerespalte zu kennzeichnen benutz man folgende zeichenfolge '--'. Um zwei Zeilen mit einander zu verbinden benutz man '&&' und schreibt anschließend den Inhalt.
Um einen Ueberstrich zu makieren benutz man nach der entsprechenden Zeichenfolge '^-', fuer Uberpunkte '^.', oder '^..'

Das Skript nimmt automatisch eine passende Formatierung der Spalten entsprechend der maximalen Anzahl an Stellen vor und nach einem Komma für jede Spalte vor.

Titelzeilen werden durch die Symbolkombination '#*' eingeleitet. Auch hier werden die Spalten durch mindestens zwei Leerzeichen und einem Tabulator unterschieden. Einige Symbole werden automatisch in entsprechende LaTeXKommandos generiert. 
Bei der Eingabe von Einheiten sei darauf zu achten, dass jede Einheit einzelstehend sei, als je von einem Leerzeichen zum nächsten Symbol getrennt sei, um uneindeutigkeiten der Bedeutung zu vemeiden.
		z.B. Meter * MillieAmper = m mA und nicht mmA, was auch MilliMeter Amper bedeuten könnte.

Kommentar Zeilen seien mit einem vorangestellten '#'zu kennzeichnen.

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

z.B.:

Eingangsdatei:

# Kommentar
#* s	m	mA	m²
1 20 33 533
2 30.2 4.3 5.33

Ausgabedatei:

\begin{tabular}{ S[table-format=1.0] S[table-format=2.1] S[table-format=2.1] S[table-format=3.2]}
\toprule
$\si{\second }$ & $\si{\meter }$ & $\si{\milli \ampere }$ & $\si{\meter ²}$ \\
\midrule
1 & 20 & 33 & 533 \\
2 & 30.2 & 4.3 & 5.33 \\
\bottomrule
\end{tabular}

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

© by Jan S. Appelt
Die Verwendung dieser Software ist nur fuer eine nicht Kommerzielle Nutzung gedacht.
Der Erwerb dieser Software ist fuer nicht kommerzielle Nutzung unentgeldlich.
