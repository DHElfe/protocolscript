sourPath = 'data.txt'
tarPath = 'data.tex'

#------------------------------------------------Dics
vUnitDic = {'m': '\\meter ', 's': '\\second ', 'g': '\\gramm ', 'A': '\\ampere ', 'V': '\\volt ', 'Ω': '\\ohm ', 'ohm': '\\ohm '}
vMessDic = {'m': '\\milli ', 'k': '\\kilo ', 'c': '\\centi ', 'M': '\\mega '}
vOtherDic = {'Δ': '\\increment '}
vGreeDic = {}

#------------------------------------------------Funktionen

def checkUnitDics(pWord):	
	if pWord in vUnitDic:
		vRet = vUnitDic[pWord]
	else:
		vRet = ''

	return vRet
def checkMessDic(pWord):
	if pWord in vMessDic:
		vRet = vMessDic[pWord]
	else:
		vRet = ''

	return vRet
def checkOtherDics(pWord):
	if pWord in vOtherDic:
		vRet = vOtherDic[pWord]
	else:
		vRet = ''

	return vRet
#--------------Ersetzt evt. das Wort fuer LaTeX
def checkWord(pWord):
	vRet = ''	
	vChange = False
	i = 0	
									# Prueft ob Einheit
	while i < len(pWord) and vRet == '':	#1. Schleifung zur Ueberpruefung, ob Einheit, von 0 - 
		temp2 = ''
		for u in range(i, len(pWord)):			#2. Schleife zur Ueberpruefung, von i -
			temp2 += pWord[u]
		vRet = checkUnitDics(temp2)					#Checkt ob momentane Sequenz ein Einheitssymbol
		if (vRet != 0 and i > 0):						# Falls ja, schaut ob vorherige Sequenz Masszahl
			temp3 = ''
			for u in range(0, i):
				temp3 += pWord[u]
			temp3 = checkMessDic(temp3)
			if(temp3 == ''):
				vRet = ''
			else:	
				vRet = temp3 + vRet
		i += 1
	if vRet != '':												# Nacharbeit, falls was gefunden
		vRet = '\\si{'+vRet+'}'
		vChange = True

						# Prueft ob anderes Symbol, welches in LaTex anders Dargestellt werden soll.
	if vRet == '':
		i = 0
		length = len(pWord)
		while i < len(pWord):								# Erste Schleife zur Prueung, ob eine LaTeX-Symbol vorhanden 0-
			while length > i:									# Zwite Schleife, length - i
				temp2 = ''
				for u in range(i, length):			# Dritte Schleife, zur Erfassung eine Sequenz
					temp2 += pWord[u]
				temp3 = checkOtherDics(temp2)
				
				if temp3 != '':									# STuff, wenn eine fuer ein Symbol passende Squenz gefunden wurde
					vRet += temp3
					pWord = pWord.replace(temp2, '', 1)
					length = len(pWord) + 1
					temp3 = ''
					vChange = True
				
				length = length - 1
			if i < len(pWord):								# Fuegt gegeb. passiertes Zeichen ein.
				vRet += pWord[i]

			i += 1
	if vRet != '' and vChange == True:		# Nach bearbeitung, falls bis jetzt was gefunden
		vRet = '' +vRet+ ''
	else:
		vRet = ''
																	# Fuegt entsprechendes LaTeX-Kommando fuer Wort an
	if vRet == '' and len(pWord) != 1:
		vRet = '\\text{' + pWord + '}'
	elif vRet == '':
		vRet = pWord

	return vRet

#---------------------------------------------Eigentliches Skript

with open(sourPath,'r') as File:
	FileText = File.readlines()

	MidString = '\\midrule\n'			# String fuer Mittelteil der Tabelle
	TopString = '\\toprule\n'			# STring fuer Oberteil der Tabelle
	vRowCount = 0				# Zaehlt die Anzahl der Spaltn

	lPreDot = []
	lPostDot = []
						# Schleife um Zeilen im eingelesenen Text durch zu gehen.
	for line in FileText:	
		temp = ''							# temporaerer String
		vSpaceCount = 0				# Zaehlt die Space's zwischen den zeichen(Auch Tabbs)
		vTempRowCount = 1			# Zaehlt die Spaltenzahl in der aktuellen Zeile
		vFirstSpaceSeq = True	# Gibt an, ob es der ersten evt. SpaceSequenze ist(wird ignoriert)
		vSpaceMode = True			# Gibt an, ob er gerade in einer SpaceSequenz ist(war)

		if len(line) >= 1 and line[0] == '#':				#Seperariert die Daten heraus.
			if len(line) >= 2 and line[1] == '*':
				ColTemp = '$'
				
				for char in line:												# Schleifewelche die Zeichen der Zeile durch geht 
					if char != '#' and char != '*':				# Sortiert Zeichen aus
						if char == ' ' or char == '\t':			# Prueft ob Space
							vSpaceMod = True							
							if char == '\t' and vSpaceCount == 0:
								vSpaceCount += 2
							else:
								vSpaceCount += 1
						else:
							if char != '\n':
														# Prueft darauf,ob eine neue Spalte vorliegt.
								if vSpaceMod == True and vSpaceCount >= 2 and vFirstSpaceSeq != True:
									if temp != '':
										ColTemp += checkWord(temp)
										temp = ''
									ColTemp += '$ & $' 
									vTempRowCount += 1
														# Preuft darauf, ob ein neues Word anfaengt
								if vSpaceCount == 1 and vFirstSpaceSeq != True:
									ColTemp += checkWord(temp)
									temp = ''

								if char == '_':
									ColTemp += checkWord(temp) + '_'
									temp = ''
								else:
									temp += char

								vSpaceCount = 0
								vSpaceMod = False
								vFirstSpaceSeq = False
							else:
								if temp != '':
									ColTemp += checkWord(temp) + '$'
									temp = ''
								ColTemp += ' \\\\\n'
								TopString += ColTemp
																				# Prueft ob aktuelle Spaltenzahl = bis jetzt gezaehlte Spaltenzahl
								if vRowCount == 0:
									vRowCount = vTempRowCount
									for i in range(0, vRowCount):		# initialisiert FormatCounter(zahehlen max. ziffern)
										lPreDot += [0]
										lPostDot += [0]
								elif vRowCount != vTempRowCount:
									print('In den Zeilen ist nicht immer die gleiche Anzahl an Spalten vorhanden.')
									print('In the Coloums arn\'t the same Number of Rows.')
#----------------------------Mid-Table-Abschnitt
		else:
			vTempPreDot = 0;
			vTempPostDot = 0;
			vDotState = 0

			for char in line:					# Schleife in die die einzelnen Zeichen einer Zeile durch gegangen werden
				if char == ' ' or char == '\t':									# Prueft ob Leerzeichen
					vSpaceMod = True					
					
					if char == '\t' and vSpaceCount == 0:					
						vSpaceCount += 2
					else:
						vSpaceCount += 1
				else:
					if char != '\n':									# Prueft ob letztes Zeichen der Zeile erreicht
																						# Prueft darauf,ob eine neue Spalte vorliegt.
						if vSpaceMod == True and vSpaceCount >= 2 and vFirstSpaceSeq != True:
							if vDotState == 0:						# Pruft PreDot Count
								if vTempPreDot > lPreDot[vTempRowCount-1]:
									lPreDot[vTempRowCount-1] = vTempPreDot
								vTempPreDot = 0
							elif vDotState == 1:					# Pruft PostDot Count
								if vTempPostDot > lPostDot[vTempRowCount-1]:
									lPostDot[vTempRowCount-1] = vTempPostDot
								vTempPostDot = 0
							
							temp += ' & ' 
							vDotState = 0
							vTempRowCount += 1
																						# Prueft ob Zahl mit Space	versehen					
						if vSpaceMod == True and vSpaceCount == 1 and vFirstSpaceSeq != True:
							if vDotState == 0:						# Pruft PreDot Count
								if vTempPreDot > lPreDot[vTempRowCount-1]:
									lPreDot[vTempRowCount-1] = vTempPreDot
								vTempPreDot = 0
							elif vDotState == 1:					# Pruft PostDot Count
								if vTempPostDot > lPostDot[vTempRowCount-1]:
									lPostDot[vTempRowCount-1] = vTempPostDot
								vTempPostDot = 0
							vDotState = 2

						if vDotState != 2:
							if char == '.' or char == ',':
								if vTempPreDot > lPreDot[vTempRowCount-1]:	# Pruft PreDot Count 
									lPreDot[vTempRowCount-1] = vTempPreDot
								vTempPreDot = 0
								vDotState = 1
							else:
								if vDotState == 0:		
									vTempPreDot += 1
								elif vDotState == 1:
									vTempPostDot += 1
						
						vSpaceCount = 0	
						vSpaceMod = False
						vFirstSpaceSeq = False
						temp += char
					else:
						if vDotState == 0 and vTempPreDot > lPreDot[vTempRowCount-1]:
							lPreDot[vTempRowCount-1] = vTempPreDot
						elif vDotState == 1 and vTempPostDot > lPostDot[vTempRowCount-1]:
							lPostDot[vTempRowCount-1] = vTempPostDot
						
						temp += ' \\\\\n'
						MidString += temp
																			# Prueft ob aktuelle Spaltenzahl = bis jetzt gezaehlte Spaltenzahl
						if vRowCount == 0:
							vRowCount = vTempRowCount
						elif vRowCount != vTempRowCount:
							print('In den Zeilen ist nicht immer die gleiche Anzahl an Spalten vorhanden.')
							print('In the Coloums arn\'t the same Number of Rows.')
						

File.close()

#-------------------------------------------Erstellt neue Datei

tempS = ''
for i in range(0, vRowCount):
	tempS += ' S[table-format={}.{}]'.format(lPreDot[i], lPostDot[i])

with open(tarPath, 'w') as SecFile:
	SecFile.write('\\begin{tabular}{'+ tempS +'}\n'+ TopString + MidString +'\\bottomrule\n\\end{tabular}')
SecFile.close()

